local yield = coroutine.yield

local N = arg[1] or 100

local tests = {
	empty_thread = function(n)
		local function f()end
		for i = 1, n do
			yield(coroutine.create(f))
		end
	end,
}

collectgarbage 'stop'
collectgarbage 'collect'

for test, testf in pairs(tests) do
	local t,i = {},1
	local gen = coroutine.wrap(testf)
	collectgarbage 'collect'
	local baseline = collectgarbage 'count'
	for obj in gen, N do
		t[i],i=obj,i+1
	end
	print(test,(collectgarbage 'count' - baseline)/N)
end
